const number = 7;
function countVowelLetters (str) {
    if (typeof str !== 'string') return 'не строка'
    if (str.length < 1) return str;
    return str.split('').filter(letter => 'аеёиоуыэюя'.includes(letter)).length;
}
console.log(countVowelLetters('бб'));
console.log(countVowelLetters(''));
console.log(countVowelLetters('Ghbdtn, z r dfv bp Hjccbb'));
console.log(countVowelLetters(number));
console.log(countVowelLetters('длинношеее'));
console.log(countVowelLetters('а'));
console.log(countVowelLetters('б'));


////////////////////////////////////////////////////////////////////
const number1 = 7;
function countVowelLetters1 (str) {
    if (typeof str !== 'string') return 'не строка'
    if (str.length < 1) return str;
    if (str.match(/[аеёиоуыэюя]/gi) === null ) return '0';
    if (str.match(/[бвгджзйклмнпрстфхцчшщ]/gi) === null || str.match(/[бвгджзйклмнпрстфхцчшщ]/gi).length === str.length ) return str.match(/[аеёиоуыэюя]/gi).length;
    return str.match(/[аеёиоуыэюя]/gi).length;
}
console.log(countVowelLetters1('бб'));
console.log(countVowelLetters1(''));
console.log(countVowelLetters1('Ghbdtn, z r dfv bp Hjccbb'));
console.log(countVowelLetters1(number1));
console.log(countVowelLetters1('длинношеее'));
console.log(countVowelLetters1('а'));
console.log(countVowelLetters1('б'));
