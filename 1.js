const num = 42;
function compress (str) {
    if (typeof str !== 'string') return 'Not a string passed as an argument!'
    const characters = new Set(str.split(''));
    let result = '';

    characters.forEach(character => {
        const characterCount = str.split(character).length - 1;
        result += character + characterCount;
    })

    return result;
}

console.log(compress('aaaabbb'));
console.log(compress(''));
console.log(compress('ab'));
console.log(compress('aaaabbbccccc'));
console.log(compress(num));


function uncompress (str) {
    if (typeof str === 'string' && str.length > 0) {
        const characters = str.match(/[a-zA-Z]\d+/g);

        if (!Array.isArray(characters)) {
            return str;
        }

        let result = '';

        for (const character of characters) {
            const letter = character.match(/[a-zA-Z]/g)[0];
            const number = character.match(/\d+/g)[0];
            result += letter.repeat(number);
        }

        return result;
    } else {
        return str;
    }
}


console.log(uncompress('a5b2c4'))
console.log(uncompress('a2b1c3'));
console.log(uncompress(''));
console.log(uncompress('ab'));
console.log(uncompress('a5b2c4'));
